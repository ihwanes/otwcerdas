from django.db import models
import os

# Create your models here.
class Document(models.Model):
    date_created = models.DateTimeField(auto_now_add=True, null=True)
    name = models.CharField(max_length=255, blank=False)
    file = models.FileField()

    def filename(self):
        return os.path.basename(self.file.name)

    def __str__(self):
        return self.name