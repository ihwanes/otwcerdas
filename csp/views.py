from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import *
from .models import *
import openpyxl
from .graph import Graph
from csp import algorithm
# Create your views here.
DOC_FILE_TYPES = ['']

def csp(request):
    if request.method == 'POST':
        file = request.FILES['uploaded-file']
        excel_data = get_excel_data(file) # 1 get_excel_data(file)
        result = assign_theme(excel_data) # 6 assign_theme(excel_data)
        return render(request, 'tescsp.html', {'result' : result}) # 17 pass data hasil proses ke page
    else:
        return render(request, 'tescsp.html')   

def get_excel_data(file):
    wb = openpyxl.load_workbook(file) # 2 buka file pakai openpyxl
    
    worksheet = wb.active # 3 get sheet excel yang active/default

    excel_data = list()
    for row in worksheet.iter_rows(): # 4 proses data dari tiap row di excel
        row_data = dict()
        row_data["nama_mhs"] = row[1].value
        row_data["domisili"] = row[2].value
        excel_data.append(row_data)
    return excel_data # 5 bentuk akhir list of dictionaries

def assign_theme(data):
    assigned_graph = init_constraint_graph() # 7 initiate graph untuk csp
    values = get_nodes_values(assigned_graph) # 12 ambil node dari graph
    for index in data:
        for val in values:
            if index["domisili"] == val:
                index["tema"] = values[val] # 15 cocokkan data domisili dengan node digraph kemudian assign value sesuai di graph
    return data # 16 return hasil penambahan key "tema" pada setiap row data

def init_constraint_graph():
    graph = Graph() # 8 bikin constraint graph
    graph = algorithm.dummy_input(graph) # 9 bikin variabel dummy di graph
    assigned_graph = algorithm.csp(graph) # 10 jalankan algoritma csp ke graph
    return assigned_graph # 11 return graph dengan node yang valuenya sudah diassign

def get_nodes_values(assigned_graph): # 13 get setiap node dari graph yg terbentuk dari algoritma csp
    result = dict()
    for node in assigned_graph.nodes:
        result[str(node.get_name())] = node.get_value()
    return result # 14 bentuk akhir: {"domisili" = "tema"}