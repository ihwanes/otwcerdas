from .node import Node

class Graph:

	def __init__(self):
		self.nodes = []

	def add_node(self, node_name):
		try:
			new_node = self.get_node(node_name)
		except:
			new_node = Node(node_name)
			self.nodes.append(new_node)

	def add_node_neighbor(self, node_name, neighbor_name):
		node = self.get_node(node_name)
		try:
			neighbor = self.get_node(neighbor_name)
		except:
			neighbor = Node(neighbor_name)
			self.nodes.append(neighbor)		
		node.add_neighbor(neighbor)
		neighbor.add_neighbor(node)

	def assign_value(self, node, value):
		node.set_value(value)

	def get_node(self, node_name):
		for node in self.nodes:
			if node.get_name() == node_name:
				return node
		raise Exception("Node is not found")

	def get_nodes(self):
		return self.nodes



