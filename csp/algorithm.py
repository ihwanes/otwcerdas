from .graph import Graph

def main():
	graph = Graph() # 1 bikin constraint graph
	graph = dummy_input(graph) # 2 bikin variabel dummy di graph
	assigned_graph = csp(graph) # 3 jalankan algoritma csp ke graph

def choose_node(nodes):
	mcv_nodes = most_constrained_var(nodes) # 8 pilih node berdasarkan mrv heuristic
	if len(mcv_nodes) > 1:
		mcv_nodes = most_constraining_var(mcv_nodes) # 9 pilih node berdasarkan degree heuristic
	choosen_node = mcv_nodes[0] # 10 sebagai tie-breaker jika ada > 1 node, pilih node pertama
	return choosen_node # 11 return node terpilih

def choose_value(node):
	choosen_value = least_constraining_val(node) 
	return choosen_value

def csp(graph):
	nodes = graph.get_nodes() # 4 assign semua node di graph ke variabel nodes
	unassigned_nodes = unassigned(nodes) # 5 inisiasi list unassigned_nodes
	while exist_nodes(unassigned_nodes): # 6 selama masih ada node yang belum diassign ...
		try:
			node = choose_node(unassigned_nodes) # 7 pilih node sesuai aturan di slide
			value = choose_value(node) # 12 pilih value untuk node terpilih sesuai aturan di slide
			graph.assign_value(node, value) # 13 assign value terpilih ke node terpilih
			print("Assign " + str(value) + " ke " + node.get_name())
			forward_check(node) # 14 lakukan forward checking terhadap node yang tadi diassign
			unassigned_nodes = unassigned(unassigned_nodes) # 15 seleksi node yang belum diassign
		except:
			raise Exception("Pemenuhan constraint tidak dapat dipenuhi")
	return graph # 16 setelah loop selesai, return graph

def exist_nodes(nodes):
	return bool(nodes)

def forward_check(node):
	neighbor_nodes = unassigned(node.get_neighbors())
	for i in range(len(neighbor_nodes)):
		if node.get_value() in neighbor_nodes[i].get_value():
			neighbor_nodes[i].get_value().remove(node.get_value())

def least_constraining_val(node):
	lcv = node.get_value()[0]
	if len(node.get_value()) > 1:		
		max_neighbor_possible_val = 0
		for val in node.get_value():
			neighbor_possible_val = 0
			neighbor_nodes = unassigned(node.get_neighbors())
			for i in range(len(neighbor_nodes)):
				for val2 in neighbor_nodes[i].get_value():
					if not val == val2:
						neighbor_possible_val += 1
			if neighbor_possible_val > max_neighbor_possible_val:
				max_neighbor_possible_val = neighbor_possible_val
				lcv = val
	return lcv

def most_constraining_var(nodes):
	mcv_nodes = [nodes[0]]
	for i in range(1,len(nodes)):
		if not nodes[i].get_degree() < mcv_nodes[0].get_degree():
			if nodes[i].get_degree() > mcv_nodes[0].get_degree():
				mcv_nodes = []
			mcv_nodes.append(nodes[i])
	return mcv_nodes

def most_constrained_var(nodes):
	mcv_nodes = [nodes[0]]
	for i in range(1,len(nodes)):
		if not nodes[i].get_rv() > mcv_nodes[0].get_rv():
			if nodes[i].get_rv() < mcv_nodes[0].get_rv():
				mcv_nodes = []
			mcv_nodes.append(nodes[i])
	return mcv_nodes

def unassigned(nodes):
	unassigned_nodes = []
	for i in range(len(nodes)):
		if not nodes[i].get_assigned():
			unassigned_nodes.append(nodes[i])
	return unassigned_nodes

def dummy_input(graph):
	graph.add_node("Bekasi, Kota")
	graph.add_node_neighbor("Bekasi, Kota","Bekasi")
	graph.add_node_neighbor("Bekasi, Kota","Depok, Kota")
	graph.add_node_neighbor("Bekasi, Kota","Bogor")
	graph.add_node_neighbor("Bekasi","Karawang")
	graph.add_node_neighbor("Bekasi","Bogor")
	graph.add_node_neighbor("Depok, Kota","Bogor")
	graph.add_node_neighbor("Bogor","Bogor, Kota")
	graph.add_node_neighbor("Bogor","Karawang")
	graph.add_node_neighbor("Bogor","Cianjur")
	graph.add_node_neighbor("Bogor","Sukabumi")
	graph.add_node_neighbor("Karawang","Subang")
	graph.add_node_neighbor("Karawang","Purwakarta")
	graph.add_node_neighbor("Cianjur","Purwakarta")
	graph.add_node_neighbor("Cianjur","Bandung Barat")
	graph.add_node_neighbor("Cianjur","Bandung")
	graph.add_node_neighbor("Cianjur","Garut")
	graph.add_node_neighbor("Cianjur","Sukabumi")
	graph.add_node_neighbor("Sukabumi","Sukabumi, Kota")
	graph.add_node_neighbor("Subang","Indramayu")
	graph.add_node_neighbor("Subang","Sumedang")
	graph.add_node_neighbor("Subang","Bandung")
	graph.add_node_neighbor("Subang","Bandung Barat")
	graph.add_node_neighbor("Subang","Purwakarta")
	graph.add_node_neighbor("Purwakarta","Bandung Barat")
	graph.add_node_neighbor("Bandung Barat","Cimahi, Kota")
	graph.add_node_neighbor("Bandung Barat","Bandung, Kota")
	graph.add_node_neighbor("Bandung Barat", "Bandung")
	graph.add_node_neighbor("Bandung","Sumedang")
	graph.add_node_neighbor("Bandung","Garut")
	graph.add_node_neighbor("Bandung","Bandung, Kota")
	graph.add_node_neighbor("Bandung","Cimahi, Kota")
	graph.add_node_neighbor("Garut","Sumedang")
	graph.add_node_neighbor("Garut","Majalengka")
	graph.add_node_neighbor("Garut","Tasikmalaya")
	graph.add_node_neighbor("Indramayu","Cirebon")
	graph.add_node_neighbor("Indramayu","Majalengka")
	graph.add_node_neighbor("Indramayu","Sumedang")
	graph.add_node_neighbor("Sumedang","Majalengka")
	graph.add_node_neighbor("Majalengka","Cirebon")
	graph.add_node_neighbor("Majalengka","Kuningan")
	graph.add_node_neighbor("Majalengka","Ciamis")
	graph.add_node_neighbor("Majalengka","Tasikmalaya")
	graph.add_node_neighbor("Tasikmalaya","Tasikmalaya, Kota")
	graph.add_node_neighbor("Tasikmalaya","Ciamis")
	graph.add_node_neighbor("Tasikmalaya","Pangandaran")
	graph.add_node_neighbor("Cirebon","Cirebon, Kota")
	graph.add_node_neighbor("Cirebon","Kuningan")
	graph.add_node_neighbor("Kuningan","Ciamis")
	graph.add_node_neighbor("Ciamis","Banjar, Kota")
	graph.add_node_neighbor("Ciamis","Pangandaran")
	graph.add_node_neighbor("Ciamis","Tasikmalaya, Kota")
	return graph