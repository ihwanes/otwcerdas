from django import forms
from csp.models import *
#DataFlair #File_Upload
class DocumentForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = ['file']

    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })