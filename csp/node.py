class Node:

	def __init__(self, name):
		self.name = name
		self.neighbors = []
		self.value = ["Kesehatan", "Ekonomi", "Pendidikan", "Pemerintahan"]
		self.assigned = False

	def add_neighbor(self, neighbor):
		self.neighbors.append(neighbor)

	def get_assigned(self):
		return self.assigned

	def get_degree(self):
		degree = 0
		for i in range(len(self.neighbors)):
			if not self.neighbors[i].assigned:
				degree += 1
		return degree

	def get_name(self):
		return self.name

	def get_neighbors(self):
		return self.neighbors

	def get_rv(self):
		return len(self.value)

	def get_value(self):
		return self.value

	def set_value(self, value):
		self.value = value
		self.assigned = True
